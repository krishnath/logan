angular.module('MyApp').controller('ResourceCtrl', function($scope, $alert, Show, $http) {
	$scope.init = function() {

		$http.get('/api/getresourceStats').success(function(data) {
			$scope.data = data.resourcestats;
			$scope.sortType = 'value';
			$scope.sortReverse = false;

		}).error(function(data, status, headers, config) {
			$scope.data = "error occured";
		});

	};

	$scope.init();
});
