google.setOnLoadCallback(function() {

});

angular.module('MyApp').controller('DashCtrl', function($scope, $alert, Show, $http) {

	$scope.init = function() {
		$scope.enabled = true;

		$http.get('/api/gethttpStats').success(function(data) {

			$scope.data = data.httpstats;
			values = [];
			values.push(new Array('HTTP status code', 'Count'));
			keys = Object.keys($scope.data);

			keys.forEach(function(k) {
				values.push(new Array(k, $scope.data[k]));
			});
			console.log(values);

			var httpdata = google.visualization.arrayToDataTable(values);
			var options = {
				title : 'HTTP status statistics',
				is3D : true,
				height : '500',

			};
			var chart = new google.visualization.BarChart(document.getElementById('httpchrtdiv'));
			chart.draw(httpdata, options);

		}).error(function(data, status, headers, config) {
			$scope.data = "error occured";
		});

		$http.get('/api/dashboardResource').success(function(data) {
			$scope.data = data.stats;
			values = [];
			values.push(new Array('Page', 'Number of Visits'));
			keys = Object.keys($scope.data);

			keys.forEach(function(k) {
				values.push(new Array(k, $scope.data[k]));
			});
			console.log(values);

			var countrydata = google.visualization.arrayToDataTable(values);
			var options = {
				title : 'Resource wise statistics',
				is3D : true,
				height : '500',

			};
			var chart = new google.visualization.BarChart(document.getElementById('resourcechrtdiv'));
			chart.draw(countrydata, options);
		}).error(function(data, status, headers, config) {
			$scope.data = "error occured";
		});

		$http.get('/api/dashboardCountry').success(function(data) {
			$scope.data = data.countrystats;
			values = [];
			values.push(new Array('Country', 'Number of Visits'));
			keys = Object.keys($scope.data);

			keys.forEach(function(k) {
				values.push(new Array(k, $scope.data[k]));
			});
			console.log(values);

			var countrydata = google.visualization.arrayToDataTable(values);
			var options = {
				title : 'Country wise usage',
				is3D : true,
				height : '500',

			};
			var chart = new google.visualization.PieChart(document.getElementById('countrychrtdiv'));
			chart.draw(countrydata, options);
			$scope.enabled = false;

		}).error(function(data, status, headers, config) {
			$scope.data = "error occured";
		});

	};

	$scope.init();
});
