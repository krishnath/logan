angular.module('MyApp').controller('HttpCtrl', function($scope, $alert, Show, $http) {
	$scope.init = function() {

		$http.get('/api/gethttpStats').success(function(data) {
			$scope.data = data.httpstats;
			$scope.sortType = 'value';
			$scope.sortReverse = false;

		}).error(function(data, status, headers, config) {
			$scope.data = "error occured";
		});
		
	};

	$scope.init();
});
